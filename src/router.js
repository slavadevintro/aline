import Vue from 'vue'
import Router from 'vue-router'
import listComments from './views/listComments'
import calculator from './views/calculator'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'listComments',
      component: listComments
    },
    {
      path: '/calculator',
      name: 'calculator',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: calculator
    }
  ]
})
