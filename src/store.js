import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {
    loadServerComment(){
      let dataServer = [];
      for ( let i=0 ; i<10 ; i++){
        dataServer.push({
          id: i+1,
          comment: 'Тестовый комментарий - ' + (i+1)
        })
      }
      return dataServer
    }
  }
})
